lua-torch-torch7 (0~20170926-g89ede3b-7) UNRELEASED; urgency=medium

  * Override dh_missing to list missing files.
  * Install missing headers and cmake config files.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 20 Sep 2018 08:56:42 +0000

lua-torch-torch7 (0~20170926-g89ede3b-6) unstable; urgency=medium

  * The move of headers e.g. TH.h breaks:
    lua-torch-images (<= 0~20170420-g5aa1881-6),
    lua-torch-nn (<= 0~20171002-g8726825+dfsg-3),
    lua-torch-cutorch (<= 0~20170911-g5e9d86c-2),
    (Closes: #908663)

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 12 Sep 2018 12:00:25 +0000

lua-torch-torch7 (0~20170926-g89ede3b-5) unstable; urgency=medium

  * Fix FTBFS (Closes: #906490)
    + Add one more include path via rules to avoid build failure.
    + Fix library path for dh_lua to avoid build failure.
    + Fix installation path to avoid build failure.
  * Remove an unused install file.
  * Bump Standards-Version to 4.2.1 (no change).
  * Re-enable arm64 architecture. (Closes: #873586)

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 31 Aug 2018 16:29:13 +0000

lua-torch-torch7 (0~20170926-g89ede3b-4) unstable; urgency=medium

  * README.Debian: Add instructions for MKL user. One needs to rebuild
    this package to utilize MKL or the unit tests would fail.
    (Closes: #897894)
  * Disable Multi-Arch support for all packages. Multi-Arch installation
    of packages produced by this source won't bring any advantage.
    (Closes: #897339)

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 10 May 2018 07:51:55 +0000

lua-torch-torch7 (0~20170926-g89ede3b-3) unstable; urgency=medium

  * Add watch file to monitor upstream master branch.
  * Bump Standards-Version to 4.1.4 . (no change)
  * Remove unused lintian overrides.

 -- Mo Zhou <cdluminate@gmail.com>  Sat, 21 Apr 2018 03:56:49 +0000

lua-torch-torch7 (0~20170926-g89ede3b-2) unstable; urgency=medium

  * Add patch to fix i386 build.
  * Enable build on i386. This unblocks migration of rdeps with Arch=all.
  * Bump debhelper compat level to 11.
  * Don't set DEB_HOST_{,MULTI}ARCH in rules.
  * Check DEB_BUILD_OPTIONS for overrided dh_auto_test target.
  * Use https URI in copyright.
  * Point Vcs-* fields to Salsa.
  * Bump Standards-Version to 4.1.3 (no change).

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 13 Feb 2018 02:51:38 +0000

lua-torch-torch7 (0~20170926-g89ede3b-1) unstable; urgency=medium

  * New upstream version 0~20170926-g89ede3b
  * Bump Standards-Version to 4.1.1 . No change needed.
  * Update my name in control and copyright.
  * Refresh symbols control file.
  * Update lintian Overrides.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 25 Oct 2017 12:09:13 +0000

lua-torch-torch7 (0~20170720-gaed3171-2) unstable; urgency=medium

  * Remove build on arm64 architecutre, which is not the priority of upstream.
  * Bump Standards-Version to 4.0.1 . (requires no change)
  * Update lintian overrides.
  * Upload to unstable.

 -- Zhou Mo <cdluminate@gmail.com>  Sat, 26 Aug 2017 02:52:22 +0000

lua-torch-torch7 (0~20170720-gaed3171-1) experimental; urgency=medium

  * Import snapshot.
  * Bump Standards-Version to 4.0.0 (requires no change).
  * Replace DEB_BUILD_OPTIONS with DH_BUILD_MAINT_OPTIONS in rules.
  * Refresh symbols.

 -- Zhou Mo <cdluminate@gmail.com>  Fri, 28 Jul 2017 16:48:46 +0000

lua-torch-torch7 (0~20170511-gae1a805-3) experimental; urgency=medium

  * Mark SSE/AVX symbols as optional.
  * Revert the removal of architecture flags in rules. These flags are
    used by the cmake-disable-simd-* patches.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 22 May 2017 07:05:35 +0000

lua-torch-torch7 (0~20170511-gae1a805-2) experimental; urgency=medium

  * Revert the removal of cmake-disable-simd-* patches.
    + patches/cmake-disable-simd-for-compatibility.patch
    + patches/cmake-disable-simd-on-arm.patch
    This package still FTBFS without the patches due to SIGILL.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 22 May 2017 03:17:10 +0000

lua-torch-torch7 (0~20170511-gae1a805-1) experimental; urgency=medium

  * Import upstream snapshot.
  * Refresh symbols control file.
  * Update lua5.1.dh-lua.conf due to upstream file rename.
  * Remove all patches:
    - patches/cmake-disable-simd-for-compatibility.patch
    - patches/cmake-disable-simd-on-arm.patch
    Now upstream is confident about the dispatcher which will select
    the best function according to runtime cpu capability detection.
    So these Debian specific patches are not needed anymore.
  * Rules: reduce compiler warning output and do grooming.

 -- Zhou Mo <cdluminate@gmail.com>  Sat, 20 May 2017 10:14:40 +0000

lua-torch-torch7 (0~20170304-g329dff5-2) experimental; urgency=medium

  * Filter architectures that are not supported by upstream.
    + Supported arch: amd64 arm64 armel armhf ppc64el kfreebsd-amd64

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 20 Mar 2017 08:19:51 +0000

lua-torch-torch7 (0~20170304-g329dff5-1) experimental; urgency=medium

  * Import upstream snapshot.
  * Refresh patch: cmake-disable-simd-for-compatibility.patch .
  * Refresh symbols control file.

 -- Zhou Mo <cdluminate@gmail.com>  Tue, 14 Mar 2017 08:45:41 +0000

lua-torch-torch7 (0~20170203-g68f34f6-1) experimental; urgency=medium

  * Import upstream snapshot.
  * Refresh symbols control file.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 06 Feb 2017 04:05:37 +0000

lua-torch-torch7 (0~20170106-gf624ae9-2) experimental; urgency=medium

  * Patch: fix arm64 build by add __aarch64__ flag.

 -- Zhou Mo <cdluminate@gmail.com>  Tue, 10 Jan 2017 07:44:27 +0000

lua-torch-torch7 (0~20170106-gf624ae9-1) experimental; urgency=medium

  * Import upstream snapshot.
  * Disable the OMAP3, OMAP4 and NEON instruction sets for compatibility.
  * Remove symbols.amd64 and symbols.i386, they are same with the master
    symbols control file after the removal of SIMD instructions.
  * Refresh symbols control file.
  * Bump debhelper compat to 10.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 09 Jan 2017 15:22:10 +0000

lua-torch-torch7 (0~20161115-g552b086-1) experimental; urgency=medium

  * Import upstream snapshot.
  * Refresh symbols control files.
  * Add the missing description in patches.
  * Fix typo in previous changelog entry.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 21 Nov 2016 03:23:09 +0000

lua-torch-torch7 (0~20161013-g4f7843e-2) experimental; urgency=medium

  * Drop -pedantic to reduce build log size.
  * Disable AVX, SSE4.2, SSE4.1, SSE3 instruction sets on amd64, and disable
    AVX and SSE* instruction sets on i386, which makes several symbols of
    optimized convolution disappeared from libTH.so . The performance gain
    from SIMD instruction sets are lost but we obtain a better compatibility.
    + Add patch: cmake-disable-simd-for-compatibility.patch
    + Refresh symbols control files.
  * Add tool script for benchmarking with upstream unittest.

 -- Zhou Mo <cdluminate@gmail.com>  Sun, 20 Nov 2016 14:05:19 +0000

lua-torch-torch7 (0~20161013-g4f7843e-1) experimental; urgency=medium

  * Import upstream snapshot (4f7843e).
  * Add autopkgtest support.
  * Refresh symbols control files.

 -- Zhou Mo <cdluminate@gmail.com>  Sun, 16 Oct 2016 06:33:05 +0000

lua-torch-torch7 (0~20161002-geb397ad-1) experimental; urgency=medium

  * Import upstream snapshot (eb397ad).
  * Remove merged patch: fix-cmake-checkfunctionexists .
  * Refresh symbols control file for amd64, i386 and any.
  * Install torch7 test scripts as examples.
  * Update README.Debian .
  * Don't ship *.c source in libtorch-th-dev package.

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 03 Oct 2016 09:01:09 +0000

lua-torch-torch7 (0~20160908-ge5ebac6-1) experimental; urgency=medium

  * Import upstream snapshot e5ebac6a3d6b14382e4641a8701f23b57e7d6e30.
  * Remove all patches that were merged to upstream.
    - fix-spelling-error
    - fix-32bit-system-abs-failure
    - TH-cmake-add-version
    - luaT-cmake-add-version
  * Use elegant dh_auto_configure instead of hardcoded commands.
  * Add patch fix-cmake-checkfunctionexists to fix cmake failure.
  * Refresh symbols control file.
  * Override lintian misreport. See #539066
  * Maintainer is Debian Science Team.
  * Put myself to Uploaders.

 -- Zhou Mo <cdluminate@gmail.com>  Sat, 10 Sep 2016 04:47:04 +0000

lua-torch-torch7 (0~20160803-g17ff317-3) experimental; urgency=medium

  * Add patch fix-32bit-system-abs-failure, changing the test number
    which exceeds the lower bound of long int.
  * Don't walk into .pc directory when grabbing files for the unit
    test, to avoid the surprise that patched scripts being overwrote
    by original ones under .pc directory.

 -- Zhou Mo <cdluminate@gmail.com>  Wed, 24 Aug 2016 13:31:55 +0000

lua-torch-torch7 (0~20160803-g17ff317-2) experimental; urgency=medium

  * Symlink the document directory to the lua directory, in order
    to fix the lua-torch-dok support.
  * Diverge the symbols control file of libTH.so for amd64 and i386,
    since SSE and AVX instruction sets are missing on other architectures.
  * Override dh_auto_test to run upstream unit test `torch.test()` .
  * Don't compress markdown document files (*.md), lua-torch-dok
    doesn't recognize compressed documentations.

 -- Zhou Mo <cdluminate@gmail.com>  Sun, 21 Aug 2016 12:53:15 +0000

lua-torch-torch7 (0~20160803-g17ff317-1) experimental; urgency=low

  * Initial release. Closes: #826532

 -- Zhou Mo <cdluminate@gmail.com>  Mon, 06 Jun 2016 03:50:31 +0000
